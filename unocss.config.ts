import {defineConfig, presetUno} from 'unocss'

export default defineConfig({
  presets: [
    presetUno({
      prefix: 'uu-',
      variablePrefix: 'uu-',
      arbitraryVariants: false,
    }),
  ],
  rules: [],
  shortcuts: [],
  theme: {
    // 解决小程序不支持 * 选择器
    preflightRoot: ['page,::before,::after'],
  },
  postprocess: [
    (obj) => {
      const remRE = /(-?[.\d]+)(rem)/g
      const pxRE = /(-?[.\d]+)(px)/g
      obj.entries.forEach((i) => {
        const value = i[1]
        if (typeof value === 'string' && remRE.test(value)) {
          i[1] = value.replace(remRE, (_, p1) => `${(p1 * 4)}rpx`)
        }
        if (typeof value === 'string' && pxRE.test(value)) {
          i[1] = value.replace(remRE, (_, p1) => `${(p1)}rpx`)
        }
      })
    },
  ],
})
