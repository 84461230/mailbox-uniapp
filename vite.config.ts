// Vite中文网：https://vitejs.cn/config/
import {ConfigEnv, loadEnv, UserConfig} from 'vite'
import {resolve} from 'path'
import uni from '@dcloudio/vite-plugin-uni'
import TransformPages from 'uni-read-pages-vite'
import UnoCSS from 'unocss/vite'

export default ({mode}: ConfigEnv): UserConfig => {
  const root = process.cwd()
  const env = loadEnv(mode, root)
  return {
    base: './',
    // 设置路径别名
    resolve: {
      alias: {
        '@': resolve('./src'),
      },
      extensions: ['.js', '.json', '.ts', '.vue'], // 使用路径别名时想要省略的后缀名，可以自己 增减
    },
    // 自定义全局变量
    define: {
      'process.env': {},
      ROUTES: (new TransformPages()).routes,
    },
    // 开发服务器配置
    server: {
      host: true,
      // open: true,
      port: env.VITE_PORT as any,
      proxy: {
        '^/(api)': {
          target: env.VITE_PROXY_TARGET,
          changeOrigin: true,
          secure: false,
        },
      },
    },
    // 构建配置
    build: {
      outDir: 'dist',
      chunkSizeWarningLimit: 1500,
      rollupOptions: {
        output: {
          entryFileNames: `assets/[name].${new Date().getTime()}.js`,
          chunkFileNames: `assets/[name].${new Date().getTime()}.js`,
          assetFileNames: `assets/[name].${new Date().getTime()}.[ext]`,
          compact: true,
        },
      },
    },
    // 插件
    plugins: [uni(), UnoCSS()],
  }
};
