import {defineStore} from 'pinia'
import useTokenStorage from '@/storage/token'
import {UserInfo} from '@/api/user-info/user-info.types'
import {user_info} from '@/api/user-info/user-info'

export const useUserInfoStore = defineStore('user-info', {
  state: () => ({
    user: null as UserInfo | null,
  }),
  actions: {
    async initUserInfo() {
      if (!this.user && useTokenStorage.getToken()) {
        try {
          await this.getUserInfo()
        } catch (e) {
          this.logout()
          return false
        }
        return true
      }
      return true
    },
    async getUserInfo() {
      if (this.user) return this.user
      try {
        const res = await user_info()
        const userInfo: UserInfo = res.data.data
        this.user = userInfo
        return userInfo
      } catch (e) {
        this.logout()
        return false
      }
    },
    async updateUserInfo() {
      const res = await user_info()
      const userInfo: UserInfo = res.data.data
      this.user = userInfo
      return userInfo
    },
    removeUserInfo() {
      this.user = null
    },
    logout() {
      this.removeUserInfo()
      useTokenStorage.removeToken()
    },
  },
})
