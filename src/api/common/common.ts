import {request} from '@/utils/http'
import {Token} from '@/storage/token'
import {FilePathTypeEnum, FileRes} from '@/api/common/common.types'
import {UnData, UnResponse} from '@uni-helper/uni-network'
import {HttpResponseData} from '@/utils/http/un'

export function weixin_login(code: string) {
  return request.post<Token>('/mailbox/user/login-by-weixin', {code}, {
    disableRefreshToken: true,
  })
}

export function refresh_token() {
  return request.post<Token>('/mailbox/user/refresh-token', {}, {
    disableRefreshToken: true,
  })
}

export function upload_file(filePath: string, pathType: FilePathTypeEnum): Promise<Required<UnResponse<HttpResponseData<FileRes>, UnData>>> {
  return request.upload<FileRes>({
    url: '/mailbox/file/upload-oss',
    name: 'file',
    filePath: filePath,
    fileType: 'image', // image, video, audio
    formData: {
      pathType,
    },
  })
}

