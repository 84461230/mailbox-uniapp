import {request} from '@/utils/http'
import {UserInfo} from '@/api/user-info/user-info.types'

export function user_info() {
  return request.get<UserInfo>('/mailbox/user/profile')
}

export function modify_user_info(data: Partial<UserInfo>) {
  return request.post('/mailbox/user/modify-profile', data)
}

