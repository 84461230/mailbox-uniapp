import {computed, ref} from 'vue'

let capsule = ref({
    area: 100,
    width: 0,
    height: 0,
  }),
  capsuleStyle = computed(() => ({
    width: `${capsule.value.width}px`,
    height: `${capsule.value.height}px`,
  }))

let clock = ref({
    width: 0,
    height: 0,
    top: 0,
    left: 0,
  }),
  clockStyle = computed(() => ({
    width: `${clock.value.width}px`,
    height: `${clock.value.height}px`,
    top: `${clock.value.top}px`,
    left: `${clock.value.left}px`,
  }))

let [timer, clientWidth] = [null as any, 0]

uni.getSystemInfo({
  //获取系统信息成功，将系统窗口的宽高赋给页面的宽高
  success: function (res) {
    clientWidth = res.windowWidth
  },
})

function clear() {
  if (timer) {
    clearTimeout(timer)
    timer = null
  }
}

function draw(thisInstance?: any) {
  if (!timer) {
    drawCapsule(thisInstance)
  }
  drawClock(thisInstance)
  timer = setTimeout(() => draw(thisInstance), 60)
}

function getUnit() {
  return +(clientWidth * capsule.value.area / 100 / 19).toFixed(0)
}

//画胶囊
function drawCapsule(thisInstance?: any) {
  //#使用 wx.createContext 获取绘图上下文 context
  const context = uni.createCanvasContext('capsule', thisInstance), unit = getUnit() //设置最小单元格大小
  if (!context) console.error('canvas id: capsule 不存在')
  // 设置capsule宽高
  capsule.value.width = unit * 16
  capsule.value.height = unit * 9
  //绿色区域
  context.setFillStyle('#09BB07')
  context.fillRect(unit * 3, unit, unit * 5, unit * 7)
  context.fillRect(unit * 2, unit * 2, unit, unit * 5)
  context.fillRect(unit, unit * 3, unit, unit * 3)
  //红色区域
  context.setFillStyle('#C40001')
  context.fillRect(unit * 8, unit, unit * 5, unit * 7) //上横线
  context.fillRect(unit * 13, unit * 2, unit, unit * 5)
  context.fillRect(unit * 14, unit * 3, unit, unit * 3)
  //黑色方块
  context.setFillStyle('#000')
  //左黑色方块
  context.fillRect(unit * 3, 0, unit * 10, unit) //上横线
  context.fillRect(unit * 2, unit, unit, unit)
  context.fillRect(unit, unit * 2, unit, unit)
  context.fillRect(0, unit * 3, unit, unit * 3)
  context.fillRect(unit, unit * 6, unit, unit)
  context.fillRect(unit * 2, unit * 7, unit, unit)
  //右黑色方块
  context.fillRect(unit * 3, unit * 8, unit * 10, unit) //下横线
  context.fillRect(unit * 13, unit, unit, unit)
  context.fillRect(unit * 14, unit * 2, unit, unit)
  context.fillRect(unit * 15, unit * 3, unit, unit * 3)
  context.fillRect(unit * 14, unit * 6, unit, unit)
  context.fillRect(unit * 13, unit * 7, unit, unit)

  setTimeout(() => context.draw(), 10) //no why
}

function drawClock(thisInstance?: any) {
  const context = uni.createCanvasContext('clock', thisInstance),
    unit = getUnit(),
    height = +(unit * 7).toFixed(2),
    width = +(unit * 7).toFixed(2)
  //设置clock宽高
  clock.value = {
    width: width,
    height: height,
    top: unit,
    left: +((clientWidth / 2 - capsule.value.width) + capsule.value.width - width / 2).toFixed(2),
  }

  if (!context) console.error('canvas id: clock 不存在')

  // 设置文字对应的半径
  const R = width / 2 - 60
  // 把原点的位置移动到屏幕中间，及宽的一半，高的一半
  context.translate(width / 2, height / 2)

  // 画外框
  function drawBackground() {
    // 设置线条的粗细，单位px
    context.setLineWidth(4)
    // 开始路径
    context.beginPath()
    // 运动一个圆的路径
    // arc(x,y,半径,起始位置，结束位置，false为顺时针运动)
    context.arc(0, 0, width / 2 - 2, 0, 2 * Math.PI, false)
    context.setFillStyle('#fff')
    context.fill()
    context.setStrokeStyle('#000')
    // 描出点的路径
    context.stroke()
    context.closePath()
  }

  // 画数字对应的点
  function drawdots() {
    for (let i = 0; i < 60; i++) {
      const rad = 2 * Math.PI / 60 * i
      const x = (R + 50) * Math.cos(rad)
      const y = (R + 50) * Math.sin(rad)
      context.beginPath()
      // 每5个点一个比较大
      if (i % 5 == 0) {
        context.arc(x, y, 2, 0, 2 * Math.PI, false)
      } else {
        context.arc(x, y, 1, 0, 2 * Math.PI, false)
      }
      context.setFillStyle('#c4c4c4')
      context.fill()
    }
    context.closePath()
  }

  // 画时针
  function drawHour(hour: number, minute: number) {
    // 保存画之前的状态
    context.save()
    context.beginPath()
    // 根据小时数确定大的偏移
    const rad = 2 * Math.PI / 12 * hour
    // 根据分钟数确定小的偏移
    const mrad = 2 * Math.PI / 12 / 60 * minute
    // 做旋转
    context.rotate(rad + mrad)
    context.setLineWidth(3)
    // 设置线条结束样式为圆
    context.setLineCap('round')
    // 时针向后延伸8个px；
    context.moveTo(0, 8)
    // 一开始的位置指向12点的方向，长度为R/2
    context.lineTo(0, -unit)
    context.stroke()
    context.closePath()
    // 返回画之前的状态
    context.restore()
  }

  // 画分针
  function drawMinute(minute: number, second: number) {
    context.save()
    context.beginPath()
    // 根据分钟数确定大的偏移
    const rad = 2 * Math.PI / 60 * minute
    // 根据秒数确定小的偏移
    const mrad = 2 * Math.PI / 60 / 60 * second
    context.rotate(rad + mrad)
    // 分针比时针细
    context.setLineWidth(3)
    context.setLineCap('round')
    context.moveTo(0, 10)
    // 一开始的位置指向12点的方向，长度为3 * R / 4
    context.lineTo(0, -2 * unit)
    context.stroke()
    context.closePath()
    context.restore()
  }

  // 画秒针
  function drawSecond(second: number, msecond: number) {
    context.save()
    context.beginPath()
    // 根据秒数确定大的偏移
    const rad = 2 * Math.PI / 60 * second
    // 1000ms=1s所以这里多除个1000
    const mrad = 2 * Math.PI / 60 / 1000 * msecond
    context.rotate(rad + mrad)
    context.setLineWidth(2)
    // 设置线条颜色为红色，默认为黑色
    context.setStrokeStyle('red')
    context.setLineCap('round')
    context.moveTo(0, 12)
    context.lineTo(0, -unit * 2.6)
    context.stroke()
    context.closePath()
    context.restore()
  }

  //画出中间那个灰色的圆
  function drawDot() {
    context.beginPath()
    context.arc(0, 0, 4, 0, 2 * Math.PI, false)
    context.setFillStyle('lightgrey')
    context.fill()
    context.closePath()
  }

  function Clock() {
    // 实时获取各个参数
    const now = new Date()
    const hour = now.getHours()
    const minute = now.getMinutes()
    const second = now.getSeconds()
    const msecond = now.getMilliseconds()
    // 依次执行各个方法
    drawBackground()
    // drawHoursNum();
    drawdots()
    drawHour(hour, minute)
    drawMinute(minute, second)
    drawSecond(second, msecond)
    drawDot()
    // 微信小程序要多个draw才会画出来，所以在最后画出
    setTimeout(() => context.draw(), 10) //no why
  }

  // 执行Clock这个方法，实际上执行了所有步骤
  Clock()
}

export {
  draw, clear, capsuleStyle, clockStyle,
}
